package com.example.luos.myapplication;
import android.content.Context;
import android.opengl.GLSurfaceView;

/**
 * Created by luos on 24/10/14.
 */
public class MyGLSurfaceView extends GLSurfaceView{

    public MyGLSurfaceView(Context context){
        super(context);

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(new MyGLRenderer());
    }
}
